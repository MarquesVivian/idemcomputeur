@if( $errors->any() )
    <div class="alert alert-danger">
        {{ $errors->first() }}
    </div>
@endif

<form action="{{ $action }}" method="POST" enctype="multipart/form-data">

    @method($method)
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <label for="title">Titre de l'article</label>
    <input class="form-control" type="text" name="title" id="title" value="{{ old('title', $article->title) }}" />
    @error('title')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label for="title">Titre de l'article</label>
    <textarea class="form-control" name="content" id="content" cols="30" rows="10">{{ old('content', $article->content) }}</textarea>
    @error('content')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label for="categories">Catégories</label>
    <select class="form-control" name="categories[]" id="categories" multiple>
        @foreach( $categories as $key => $value )
            <option value="{{ $key }}" {{ in_array($key, $article->categories()->pluck('categories.id')->toArray()) ? 'selected' : '' }}>{{ $value }}</option>
        @endforeach
    </select>
    @error('categories')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label for="categories">Auteur</label>
    <select class="form-control" name="author_id" id="author_id">
        @foreach( $users as $key => $value )
            <option value="{{ $key }}"{{ $key === $article->author_id ? ' selected' : '' }}>{{ $value }}</option>
        @endforeach
    </select>
    @error('categories')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <label for="image">Image</label>
    <input class="form-control" type="file" name="image" id="image" />

    <button type="submit" class="btn btn-success">Soumettre</button>
</form>

