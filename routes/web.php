<?php

use App\Http\Controllers\RolesController;
use App\Http\Controllers\CrudController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'

])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});

Route::prefix('admin')->group(function(){
    Route::get('crud', [CrudController::class, 'index'])->name('crudcontroller');
    Route::prefix('roles')->group(function(){

        Route::get('', [RolesController::class, 'index'])->name('admin.roles.index');

        Route::prefix('crud')->group(function(){

            Route::get('create', [RolesController::class, 'create'])->name('admin.roles.create');
            Route::post('create', [RolesController::class, 'store'])->name('admin.roles.store');

            Route::get('edit/{role}', [RolesController::class, 'index'])->name('admin.roles.edit');
            Route::put('update/{role}', [RolesController::class, 'index'])->name('admin.roles.update');
        });

    });
    
    Route::get('users', [UsersController::class, 'index'])->name('admin.users.index');
});

