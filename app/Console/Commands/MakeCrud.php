<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

use function PHPUnit\Framework\directoryExists;

class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud {name} {--model} {--migration}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permet de générer un crud directement';

    public array $fields = [];

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $fields = $this->ask('Quels seront les champs présents dans votre formulaire ?');
        $this->fields = (explode(',',$fields));

        $this->generateController();
        $this->generateView();
        $this->generateModel();

        if( $this->option('migration') ) {
            $this->call('make:migration', [
                'name' => 'create_' . Str::snake($this->argument('name')) . 's' . '_table',
                '--create' => Str::snake($this->argument('name')) . 's',
            ]);
        }

        return 0;
    }

    /**
     * Génère le Model
     */
    public function generateModel()
    {
        $fillable = "";
        foreach( $this->fields as $field ) {
            $fillable .= "'" . $field . "' ," . "\n";
        }

        $stub = file_get_contents(resource_path('stubs/Model.stub'));
        $modelTemplate = str_replace(
            [
                '{{ namespace }}',
                '{{ fillable }}',
                '{{ class }}'
            ],
            [
                'App\Models',
                $fillable,
                ucfirst($this->argument('name'))
            ],
            $stub
        );
        //dd($fillable);
        $modelName = ucfirst($this->argument('name')) . '.php';
        if( !file_exists(app_path('Models/' . $modelName)) ) {
            file_put_contents(app_path('Models/' . $modelName), $modelTemplate);
        }
    }

    /**
     * Génère le Controller
     */
    public function generateController()
    {

        $fieldsRules = "";
        foreach( $this->fields as $field ) {
            $fieldsRules .= "'" . $field . "' => 'required'," . "\n";
        }

        $stub = file_get_contents(resource_path('stubs/Controller.stub'));
        $controllerTemplate = str_replace([
            '{{ namespace }}',
            '{{ model }}',
            '{{ modelVariable }}',
            '{{ fieldRules }}'
        ], [
            'App\Http\Controllers\Admin',
            ucfirst($this->argument('name')),
            strtolower($this->argument('name')),
            $fieldsRules
        ], $stub);

        $controllerName = ucfirst($this->argument('name')) . 'sController.php'; // => 'UsersController';
        //dd( $controllerTemplate);

        if( !file_exists(app_path('Http/Controllers/' . $controllerName)) ) {
            file_put_contents(app_path('Http/Controllers/' . $controllerName), $controllerTemplate);
        }
    }

    public function generateView()
    {
        $fillable = "";
        foreach( $this->fields as $field ) {
            $fillable .= "'" . $field . "' ," . "\n";
        }

        $stub = file_get_contents(resource_path('stubs/Index.stub'));
        $modelTemplate = str_replace(//pour le index
            [
                '{{ name }}',
                '{{ fillable }}',
                '{{ class }}'
            ],
            [
                'ressources\admin\Views',
                $fillable,
                ucfirst($this->argument('name'))
            ],
            $stub
        );

        $stubCreate = file_get_contents(resource_path('stubs/Create.stub'));
        $modelTemplate = str_replace(//pour le create
            [
                '{{ name }}',
                '{{ fillable }}',
                '{{ class }}'
            ],
            [
                'ressources\admin\Views',
                $fillable,
                ucfirst($this->argument('name'))
            ],
            $stub
        );

        $stubEdit = file_get_contents(resource_path('stubs/Edit.stub'));
        $modelTemplate = str_replace(//pour le edit
            [
                '{{ name }}',
                '{{ fillable }}',
                '{{ class }}'
            ],
            [
                'ressources\admin\Views',
                $fillable,
                ucfirst($this->argument('name'))
            ],
            $stub
        );


        //dd(ucfirst($this->argument('name')));
        $modelName = $this->argument('name') ;
        if(!file_exists(resource_path('views/admin/'. $modelName.'s'))){
            mkdir(resource_path('views/admin/'. $modelName .'s'));
            mkdir(resource_path('views/admin/'. $modelName .'s/crud'));
            file_put_contents(resource_path('views/admin/'. $modelName .'s/crud/create.blade.php'), $modelTemplate);
            file_put_contents(resource_path('views/admin/'. $modelName .'s/crud/edit.blade.php'), $modelTemplate);
            file_put_contents(resource_path('views/admin/'. $modelName .'s/crud/form.blade.php'), $modelTemplate);
        }
        if( !file_exists(resource_path('views/admin/' . $modelName . 's/index.blade.php')) ) 
        {
            file_put_contents(resource_path('views/admin/' . $modelName . 's/index.blade.php'), $modelTemplate);
        }
        
    
    }


}

