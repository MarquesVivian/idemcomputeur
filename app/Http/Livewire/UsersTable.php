<?php

namespace App\Http\Livewire;

use App\Models\User;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\LabelColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;

class UsersTable extends LivewireDatatable
{
    public $model = User::class;


    public function columns()
    {
        /*return[
            NumberColumn::name('id')
                ->label('ID'),

            LabelColumn::name('name')
                ->defaultSort('asc')
                ->group('group1')
                ->searchable()
                ->filterable(),

                LabelColumn::name('email')
                ->defaultSort('asc')
                ->group('group1')
                ->searchable()
                ->hideable()
                ->filterable()

        ];*/

        return [
            NumberColumn::name('id')
                ->label('ID')
                ->linkTo('job', 6),

            BooleanColumn::name('email_verified_at')
                ->label('Email Verified')
                ->filterable(),

            Column::name('name')
                ->defaultSort('asc')
                ->group('group1')
                ->searchable()
                ->hideable()
                ->filterable(),

            Column::name('email')
                ->label('Planet')
                ->group('group1')
                ->searchable()
                ->hideable(),

                

            // Column that counts every line from 1 upwards, independent of content
            (new LabelColumn())
                ->label('My custom heading')
                ->content('This fixed string appears in every row'),
        ];
    }
}