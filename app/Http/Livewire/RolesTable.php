<?php

namespace App\Http\Livewire;

use App\Role;
use Mediconesystems\LivewireDatatables\BooleanColumn;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\LabelColumn;
use Mediconesystems\LivewireDatatables\NumberColumn;

class RolesTable extends LivewireDatatable
{
    public $model = Role::class;

    public function columns()
    {
        return[
            NumberColumn::name('id')
                ->label('ID'),

            Column::name('name')
                ->defaultSort('asc')
                ->group('group1')
                ->searchable()
                ->filterable(),

        ];
    }
}